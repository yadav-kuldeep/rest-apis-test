package com.noosyntech.api.test;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.*;

/**
 * Created by ni3 on 9/20/17.
 */
public class BasicTest {
    private String token;
    private String baseUrl = "http://api.frood.tech";
    private int OK = 200;
    private int BAD_REQUEST = 400;
    private int INTERNAL_SERVER = 500;
    private String XTOKEN_KEY = "X-TOKEN";

    @Before
    public void setup(){
        RestAssured.baseURI = baseUrl;
        RestAssured.basePath = "/v1";
        Map<String, String> credentials = new HashMap<String, String>();
        credentials.put("userName", "kuldeep.yadav@noosyntech.in");
        credentials.put("password", "789@123");
        token = given().contentType(ContentType.JSON).body(credentials)
                .when()
                .post("/login")
                .then().statusCode(OK).extract().path("X-TOKEN");
    }

    @Test
    public void testColumnsApi() throws IOException {
        try(FileInputStream fos = new FileInputStream(new File(getClass().getClassLoader().getResource("columns.json").getPath()))){
            Map<String,String> headers = new HashMap<>();
            headers.put(XTOKEN_KEY, token);
            given()
                    .headers(headers)
                    .contentType(ContentType.JSON)
                    .body(IOUtils.toByteArray(fos))
                    .when()
                    .put("/inventory/columns")
                    .then().assertThat().statusCode(OK);
        }

    }
}
