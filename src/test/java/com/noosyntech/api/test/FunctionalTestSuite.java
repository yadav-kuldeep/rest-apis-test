package com.noosyntech.api.test;

import com.noosyntech.api.test.sku.SkusTest;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.apache.http.HttpStatus;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;

@RunWith(Suite.class)
@Suite.SuiteClasses(SkusTest.class)
public class FunctionalTestSuite {
    @BeforeClass
    public static void setup(){
        RestAssured.baseURI = "http://api.frood.tech";
        RestAssured.basePath = "/v1";
        RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();
        Map<String, String> credentials = new HashMap<String, String>();
        credentials.put("userName", "kuldeep.yadav@noosyntech.in");
        credentials.put("password", "789@123");
        String token = given().contentType(ContentType.JSON).body(credentials)
                .when()
                .post("/login")
                .then().statusCode(HttpStatus.SC_OK).extract().path(Constants.XTOKEN_KEY);
        Context.getInstance().setToken(token);
    }
}
