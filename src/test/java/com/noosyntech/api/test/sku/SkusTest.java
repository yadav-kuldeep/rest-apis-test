package com.noosyntech.api.test.sku;

import com.noosyntech.api.test.Constants;
import com.noosyntech.api.test.Context;
import com.noosyntech.api.test.FileUtil;
import com.noosyntech.api.test.tests.GetFunctionalTests;
import com.noosyntech.api.test.tests.PostFunctionalTests;
import io.restassured.http.ContentType;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpStatus;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import static org.hamcrest.Matchers.*;

import static io.restassured.RestAssured.given;

public class SkusTest {
    String token;
    Map<String, String> headers;

    @Before
    public void setup(){
        token = Context.getInstance().getToken();
        headers = new HashMap<>();
        headers.put(Constants.XTOKEN_KEY, token);
    }

    @Test
    @Category(GetFunctionalTests.class)
    public void testAllSkusApi() throws IOException {
        given()
                .headers(headers)
                .get("/skus")
                .then()
                .assertThat().statusCode(HttpStatus.SC_OK);
    }

    @Test
    @Category(PostFunctionalTests.class)
    public void testSkuPostWithSimpleDataApi() throws IOException {
        try(FileInputStream fos = new FileInputStream(new File(FileUtil.getFilePath(getClass(), "skus/sku_post_simple.json")))){
            given()
                    .headers(headers)
                    .contentType(ContentType.JSON)
                    .body(IOUtils.toByteArray(fos))
                    .when()
                    .post("/sku")
                    .then().assertThat().statusCode(HttpStatus.SC_CREATED)
                    .assertThat()
                    .body("id", equalTo("KFP5"))
                    .log().ifStatusCodeIsEqualTo(HttpStatus.SC_BAD_REQUEST);

        }
    }
}
