package com.noosyntech.api.test;

public class FileUtil {
    public static String getFilePath(Class clazz, String relativePath){
        return clazz.getClassLoader().getResource(relativePath).getPath();
    }
}
