package com.noosyntech.api.test;

import com.noosyntech.api.test.sku.SkusTest;
import com.noosyntech.api.test.tests.PostFunctionalTests;
import org.junit.experimental.categories.Categories;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Categories.class)
@Categories.IncludeCategory(PostFunctionalTests.class)
@Suite.SuiteClasses(SkusTest.class)
public class PostFunctionalTestSuite extends FunctionalTestSuite{
}
