package com.noosyntech.api.test;

public class Context {
    private static Context context;
    private String token;
    private Context(){}

    public static Context getInstance(){
        if(context == null){
            context = new Context();
        }
        return context;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
